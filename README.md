# Tarea_sem_4

## Interactividad

### Manejo de eventos

Uno de los eventos más usados es el keyup que sirve cuando requerimos que el usuario digite una tecla para ejecutar una acción como sumar, restar, mostrar algo en pantalla, etc. Es necesario el uso de la directiva v-on para asignar un evento como keyup. Además v-on: cuenta con la abreviación @, de manera que se hace más limpio el código. Así pues, se escribe @evento = "acción" donde la acción requerida puede definirse explícitamente dentro de las comillas o puede allí colocarse solamente el nombre de una función que definirá la acción posteriormente dentro de methods y dentro de la instancia Vue. Esto último es lo que se denomina manejo de eventos a través de métodos.

Ejemplo: @click = "upvotes++"

#### Manejo de eventos a través de métodos

Con el fin de hacer más entendible el código se usan los métodos: bloques de código que pueden incluir varias funciones. Para ello se utiliza la palabra methods y dentro de sus llaves se colocan los nombres de las funciones que serán llamadas en otra parte del código y, lógicamente, cada una contendrá código que defina la acción deseada como sumar votos, por ejemplo. Cabe mencionar que es nesesario usar 'this.variable' si variable no se encuentra dentro de métodos sino en data para que así pueda ser reconocida dentro de methods.

Ejemplo: @click = "votes" , donde votes representa una función que acumula votos.

        methods: {
            votes: function () {
                ...sumando votos...
            }
        }

#### Modificadores de eventos

Sirven para cambiar el comportamiento por defecto de un evento, por ejemplo, @click.prevent. Aquí el modificador es .prevent y sirve para evitar que se recargue la página.
Para v-on:
                    * .prevent
                    * .stop
                    * .capture
                    * .self

### Modificadores de tecla

Se usan conjuntamente con el evento keyup; como se dijo previamente, keyup se usa para cuando el usuario deba presionar una tecla por lo cual es necesario establecer cuál será la tecla (enter, space, esc, etc). Para esto último se usa un modificador de tecla, así por ejemplo: @keyup.delete="funcion", @keyup.enter="f...", etc.

### Propiedades computadas

Son similares a los métodos y ente caso se utiliza la palabra 'computed' dentro de la instancia Vue. La diferencia entre computed  y methods se basa en que la primera no se ejecuta si los valores de las dependencias no han variado. 

        computed : {
            suma: function () {
                ...
                return sum
            }
        }

###### Método sort()

Sirve para ordenar los elementos de un arreglo. Funciona de manera que calcula la diferencia entre dos elementos del arreglo, retorna dicho resultad0 y dependiendo de ello (+,-,0) entiende cuál es mayor o menor.

        computed: {
            alcalde: function(){
                var candidato_ord = this.candidato.sort(function(a,b){
                    return b.votos-a.votos;
                });
                return candidato_ord[0]
            }
        }

En el ejemplo dado el arreglo se llama 'candidato' y según la cantidad de votos formando un nuevo arreglo 'candidato_ord'. Finalmente para mostrar al candidato de mayor cantidad de votos se retorna al candidato de la posición cero ya que se ordenó descendentemente.

###### Método map()

Sirve para crear un nuevo arreglo a partir de los elementos de un primer arreglo según una regla de correspondencia.

Ejemplo:

        cero: function() {
                this.candidato = this.candidato.map(function(i){
                    i.votos = 0;
                    return i
                })
        }

En el ejemplo dado el arreglo inicial se llama 'candidato' y se creará uno nuevo (en este caso con el mismo nombre, es decir, se sobreeescribirán los nuevos elementos sobre los primeros) a partir de sus elementos según la regla siguiente: los votos de cada candidato se harán cero.
